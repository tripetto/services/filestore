# Filestore
Service for storing files in Google Cloud Platform (GCP). It uses GCP App Engine to run and GCP Storage for storing files.

## Installation
Update your GCP key in the file `./gcp.key`.

Make sure you have a file `./nodemon.json` with the following contents:
```
{
  "env": {
    "TRIPETTO_APP_PK": "a-random-secret",
    "GOOGLE_APPLICATION_CREDENTIALS": "./gcp.key",
    "GOOGLE_CLOUD_PROJECT": "google-cloud-project"
  },
  "watch": ["dist/"],
  "delay": "1000"
}
```

Run `npm i` to install the required packages. Start the test server with `npm test`.

## Authentication
The service handles authentication via JWT tokens. Files can be uploaded with a valid `TRIPETTO-COLLECTOR-TOKEN` (the same token required to run the collector within the [Tripetto](https://tripetto.app) app).

To confirm, download or delete files requires a `TRIPETTO-ATTACHMENT-TOKEN`.

