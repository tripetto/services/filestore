export interface IMetadata {
    filename?: string;
    mimetype?: string;
    encoding?: string;
    owner?: string;
    definition?: string;
    timeCreated?: string;
    confirmed?: string;
}
