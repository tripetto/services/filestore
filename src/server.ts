import express from "express";
import { FileController } from "./controllers";

export class Server {
    public server: express.Application;
    public port: number;

    static start(port?: string): void {
        const server = new Server((port && parseInt(port, 10)) || 8001);
        server.listen();
    }

    constructor(port: number) {
        this.server = express();

        this.server.use(express.json());
        this.server.set("trust proxy", true);
        this.server.disable("x-powered-by");

        const fileController = new FileController();
        this.server.use("/", fileController.router);

        this.server.use(this.errorHandler());
        this.server.use(this.notFoundHandler());

        this.port = port;
    }

    private errorHandler(): express.ErrorRequestHandler {
        return (err: { status?: number; code?: number }, req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (err.status) {
                res.status(err.status);
            } else if (err.code) {
                res.status(err.code);
            } else {
                res.status(500);
            }
            res.send();
        };
    }

    private notFoundHandler(): express.RequestHandler {
        return (request: express.Request, response: express.Response) => {
            response.sendStatus(404).send("Sorry mate!");
        };
    }

    public listen(): void {
        this.server.listen(this.port, () => {
            console.log(`Server listening`);
        });
    }
}
