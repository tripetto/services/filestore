import { Storage } from "@google-cloud/storage";
import { ENV, GOOGLE_CLOUD_PROJECT } from "./settings";
import { Readable } from "stream";
import { Metadata } from "@google-cloud/common";
import { IMetadata } from "./metadata";

export interface IHasAccessResult {
    isAccessAllowed: boolean;
    metadata?: IMetadata;
}

const bucket = new Storage({
    projectId: GOOGLE_CLOUD_PROJECT,
}).bucket(`${GOOGLE_CLOUD_PROJECT}-${ENV}`);

export function createWriteStream(filePath: string, metadata: IMetadata): NodeJS.WritableStream {
    return bucket.file(filePath).createWriteStream({ resumable: false, metadata: { metadata } });
}

async function fileExists(path: string): Promise<boolean> {
    return bucket
        .file(path)
        .exists()
        .then(([exists]: [boolean]) => exists);
}

export async function hasPublicAccess(path: string, user: string, definition: string): Promise<IHasAccessResult> {
    return getMetadata(path).then((metadata?: IMetadata) =>
        (metadata && metadata.owner && metadata.owner === user && metadata.definition && metadata.definition === definition) === true
            ? { isAccessAllowed: true, metadata }
            : { isAccessAllowed: false, metadata: undefined }
    );
}

export async function hasPrivateAccess(path: string, user: string, keepAvailable?: number): Promise<IHasAccessResult> {
    return getMetadata(path).then((metadata?: IMetadata) => {
        return (metadata &&
            metadata.owner &&
            ((user && metadata.owner === user) ||
                (keepAvailable &&
                    !user &&
                    metadata.confirmed &&
                    new Date(metadata.confirmed).getTime() > new Date().getTime() - keepAvailable))) === true
            ? { isAccessAllowed: true, metadata }
            : { isAccessAllowed: false, metadata: undefined };
    });
}

async function getMetadata(path: string): Promise<IMetadata | undefined> {
    return fileExists(path).then((exists: boolean) => {
        if (exists) {
            return (
                bucket
                    .file(path)
                    .getMetadata()
                    // tslint:disable-next-line:no-any
                    .then(([values, _]: [Metadata, any]) => {
                        return { ...values.metadata, timeCreated: values.timeCreated } as IMetadata;
                    })
            );
        }
        return undefined;
    });
}

export async function confirm(filename: string, newFilename: string): Promise<void> {
    return bucket
        .file(filename)
        .move(newFilename)
        .then(() =>
            bucket
                .file(newFilename)
                .setMetadata({
                    metadata: {
                        confirmed: new Date().toISOString(),
                    },
                })
                .then(() => Promise.resolve())
        );
}

export function createReadStream(path: string): Readable {
    return bucket.file(path).createReadStream();
}

export async function deletePublicFile(filename: string, user: string, definition: string): Promise<boolean> {
    return hasPublicAccess(filename, user, definition).then(async (result: IHasAccessResult) => {
        if (result.isAccessAllowed === true) {
            return bucket
                .file(filename)
                .delete()
                .then(() => true);
        } else {
            return false;
        }
    });
}

export async function deletePrivateFile(filename: string, user: string): Promise<boolean> {
    return hasPrivateAccess(filename, user).then(async (result: IHasAccessResult) => {
        if (result.isAccessAllowed === true) {
            return bucket
                .file(filename)
                .delete()
                .then(() => true);
        } else {
            return false;
        }
    });
}
