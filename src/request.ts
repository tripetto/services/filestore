import express from "express";

export interface ICollectorTokenRequest extends express.Request {
    token: ICollectorToken;
}
export interface ICollectorToken {
    type?: "collect";
    user: string;
    definition: string;
}
export interface IFileTokenRequest extends express.Request {
    token: IFileToken;
}
export interface IFileToken {
    name: string;
    user: string;
}
export interface IDownloadFileTokenRequest extends express.Request {
    token: IFileToken;
}
