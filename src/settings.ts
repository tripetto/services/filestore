if (!process.env.TRIPETTO_APP_PK) {
    throw new Error("Tripetto File Service: The environment variable 'TRIPETTO_APP_PK' is not defined.");
}
if (!process.env.GOOGLE_CLOUD_PROJECT) {
    throw new Error("Tripetto File Service: The environment variable 'GOOGLE_CLOUD_PROJECT' is not defined.");
}

export const TRIPETTO_APP_PK = process.env.TRIPETTO_APP_PK;
export const ENV =
    process.env.TRIPETTO_APP_ENV === "production" ? "production" : process.env.TRIPETTO_APP_ENV === "staging" ? "staging" : "development";
export const GOOGLE_CLOUD_PROJECT = process.env.GOOGLE_CLOUD_PROJECT;
export const MAX_FILEUPLOAD_SIZE = 10 * 1024 * 1024;
