import busboy from "busboy";
import cors from "cors";
import crypto from "crypto";
import express from "express";
import { expressjwt } from "express-jwt";
import * as bucket from "../bucket";
import { ICollectorTokenRequest, IDownloadFileTokenRequest, IFileTokenRequest } from "../request";
import { ENV, MAX_FILEUPLOAD_SIZE, TRIPETTO_APP_PK } from "../settings";

const HEADER_COLLECTOR_TOKEN = "Tripetto-Runner-Token";
const HEADER_FILE_TOKEN = "Tripetto-Attachment-Token";
const PATH_ROOT = "/";
const PATH_UNCONFIRMED = "/unconfirmed";
const KEEP_DOWNLOAD_AVAILABLE = 1000 * 60 * 60 * 24;

export class FileController {
    public router = express.Router();

    constructor() {
        this.initializeRoutes();
    }

    private async initializeRoutes(): Promise<void> {
        /* without cors */
        this.router.patch(PATH_ROOT, this.authenticate(HEADER_FILE_TOKEN), this.confirm());
        this.router.delete(PATH_ROOT, this.authenticate(HEADER_FILE_TOKEN), this.deleteConfirmed());
        this.router.get(PATH_ROOT, this.authenticate(HEADER_FILE_TOKEN), this.download());

        /* with cors */
        this.router.options(PATH_ROOT, cors<express.Request>());
        this.router.post(PATH_ROOT, this.authenticate(HEADER_COLLECTOR_TOKEN), cors(), this.upload());

        this.router.options([PATH_UNCONFIRMED, `${PATH_UNCONFIRMED}/:file`], cors<express.Request>());
        this.router.delete(PATH_UNCONFIRMED, this.authenticate(HEADER_COLLECTOR_TOKEN), cors(), this.deleteUnconfirmed());
        this.router.get(`${PATH_UNCONFIRMED}/:file`, this.authenticate(HEADER_COLLECTOR_TOKEN), cors(), this.downloadUnconfirmed());
    }

    private authenticate(header: string): express.RequestHandler {
        return expressjwt({
            algorithms: ["HS256"],
            secret: TRIPETTO_APP_PK,
            getToken: (req: express.Request) => req.header(header),
            credentialsRequired: true,
            requestProperty: "token",
        });
    }

    private deleteUnconfirmed(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const file = req.body.file;
            if (!file) {
                res.sendStatus(400);
                return;
            }

            const token = (req as ICollectorTokenRequest).token;

            bucket
                .deletePublicFile(this.getUnconfirmedPath(file), token.user, token.definition)
                .then((isSucceeded: boolean) => res.sendStatus(isSucceeded === true ? 200 : 404))
                .catch(next);
        };
    }

    private deleteConfirmed(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const token = (req as IFileTokenRequest).token;
            bucket
                .deletePrivateFile(token.name, token.user)
                .then((isSucceeded: boolean) => res.sendStatus(isSucceeded === true ? 200 : 404))
                .catch(next);
        };
    }

    private upload(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            const token = (req as ICollectorTokenRequest).token;
            if (!token.type || token.type !== "collect") {
                res.sendStatus(401);
                return;
            }

            const uploadStream = busboy({ headers: req.headers, limits: { fileSize: MAX_FILEUPLOAD_SIZE } });
            uploadStream.on("file", (fieldName, file, info) => {
                const name = this.createFilename(info.filename);
                const unconfirmedFilename = this.getUnconfirmedPath(name);
                let isFileTooLarge = false;

                const writeStream = bucket.createWriteStream(unconfirmedFilename, {
                    filename: info.filename,
                    encoding: info.encoding,
                    mimetype: info.mimeType,
                    owner: token.user,
                    definition: token.definition,
                });

                writeStream.on("unpipe", () => {
                    if (isFileTooLarge === true) {
                        bucket
                            .deletePublicFile(unconfirmedFilename, token.user, token.definition)
                            .catch((err: Error) => this.logError(err));
                    }
                });
                writeStream.on("error", (err: Error) => {
                    this.log(`writestream.error`);
                    this.logError(err);
                });
                writeStream.on("finish", () => {
                    if (isFileTooLarge === true) {
                        res.sendStatus(413);
                    } else {
                        const data = { name };
                        res.status(200).json(data);
                    }
                });
                file.on("limit", () => {
                    isFileTooLarge = true;
                });
                file.on("error", (err: Error) => {
                    this.log("file.error");
                    this.logError(err);
                });
                file.pipe(writeStream);
            });
            return req.pipe(uploadStream);
        };
    }

    private download(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const token = (req as IDownloadFileTokenRequest).token;

            bucket
                .hasPrivateAccess(token.name, token.user, KEEP_DOWNLOAD_AVAILABLE)
                .then((result: bucket.IHasAccessResult) => {
                    if (result.isAccessAllowed === true) {
                        res.setHeader(
                            "content-disposition",
                            `inline; filename=${result.metadata && encodeURIComponent(result.metadata.filename || "")}`
                        );
                        res.contentType((result.metadata && result.metadata.mimetype) || "application/octet-stream");
                        bucket
                            .createReadStream(token.name)
                            .on("error", (err: Error) => {
                                next(err);
                            })
                            .pipe(res);
                    } else {
                        res.sendStatus(404);
                    }
                })
                .catch(next);
        };
    }

    private downloadUnconfirmed(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const file = req.params.file;
            if (!file) {
                res.sendStatus(400);
                return;
            }

            const token = (req as ICollectorTokenRequest).token;
            const unconfirmedFilename = this.getUnconfirmedPath(file);
            bucket
                .hasPublicAccess(unconfirmedFilename, token.user, token.definition)
                .then((result: bucket.IHasAccessResult) => {
                    if (result.isAccessAllowed === true) {
                        res.attachment(result.metadata && result.metadata.filename);

                        bucket
                            .createReadStream(unconfirmedFilename)
                            .on("error", (err: Error) => {
                                next(err);
                            })
                            .pipe(res);
                    } else {
                        res.sendStatus(404);
                    }
                })
                .catch(next);
        };
    }

    private confirm(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const token = (req as IFileTokenRequest).token;
            const file = token.name;
            const unconfirmedFile = this.getUnconfirmedPath(token.name);

            bucket
                .hasPrivateAccess(unconfirmedFile, token.user)
                .then((result: bucket.IHasAccessResult) => {
                    if (result.isAccessAllowed === true) {
                        bucket.confirm(unconfirmedFile, file).then(() => res.sendStatus(200));
                    } else {
                        res.sendStatus(404);
                    }
                })
                .catch(next);
        };
    }

    private createFilename(originalName: string): string {
        return this.createHash(`${Date.now()}-${originalName}`);
    }

    private getUnconfirmedPath(filename: string): string {
        return `_unconfirmed/${filename}`;
    }

    private createHash(data: string): string {
        const hash = crypto.createHash("sha256");
        hash.update(data);
        return hash.digest("hex");
    }

    private log(message: string): void {
        if (ENV === "development") {
            console.log(message);
        }
    }

    private logError(err: Error): void {
        console.error(err);
    }
}
